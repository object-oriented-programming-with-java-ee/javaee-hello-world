# JavaEE Hello World

To be able to run this code you need to:

1. Install [Apache Tomcat](https://tomcat.apache.org/)
2. Enable "Web, XML, Java EE and OSGi Enterprise Development" in Eclipse ([see here](http://www.programmersought.com/article/3367123/))
3. Configure the admin port of Tomcat in Eclipse ([see here](https://www.codejava.net/servers/tomcat/how-to-change-port-numbers-for-tomcat-in-eclipse))

When that is in place you can start the application in Eclipse and visit [http://localhost:8080/HelloWorldEE/](http://localhost:8080/HelloWorldEE/)